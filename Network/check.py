import logging
from os import system


def check_ip(list: str) -> bool:
    """
    This function parse the ip_list.txt file, put\
        the IP adresses into a list, ping all the IPs present\
        into the list, print if the IPs are UP or DOWN\
        and finally log the results into the check_ip.log file.
    """
    test_passed = True
    logging.basicConfig(level=logging.DEBUG,
                        filename='check_ip.log',
                        format='%(asctime)s :: %(levelname)s :: %(funcName)s :: %(message)s')
    try:
        with open(list, 'r') as f:
            list_of_ip = f.read().splitlines()
    except Exception as error:
        print('Error accessing ip_list.txt file.')
        exit()

    for ip in list_of_ip:
        to_ping = f'ping -c 1 -q {ip} >/dev/null 2>/dev/null'
        res = system(to_ping)
        if res:
            ip_down = f'{ip} DOWN'
            print(ip_down)
            logging.warning(ip_down)
            test_passed = False
        else:
            ip_up = f'{ip} UP'
            print(ip_up)
            logging.info(ip_up)
    return (test_passed)
