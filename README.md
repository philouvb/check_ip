# Ping a list of IP addresses

This package was developed with the goal to be used in a test bench with many devices connected. Using this package it was easier at the begining of the test sequence to see if all devices were present !

The list of IPs to be checked is to be placed into the *ip_list.txt* file one IP per line.

To keep track of the IPs status, connections status are logged into a log file.

> :Warning: **This package is due to work only on linux based systems**. However it would be easy to modify it to work with any OS.