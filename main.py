from Network.check import check_ip


if __name__ == '__main__':
    passed = check_ip('ip_list.txt')
    if passed:
        print('All IPs are OK')
    else:
        print('One or more IPs are not responding check log file')
